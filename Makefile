##################################
# e8080 emulator Makefile recipe #
##################################

#--- Environment
E8080_ENV=-DE8080_SEMVER -DE8080_MAJOR=0 -DE8080_MINOR=1 -DE8080_PATCH=0
E8080_PWD=$(shell pwd)

#--- Toolchain
CC=$(shell which gcc)
ENTRYPOINT=e8080.o
TENTRYPOINT=testing.o
OBJS=error.o cpu.o io.o disassembler.o
TOBJS=disassembler_test.o util_test.o
CFLAGS=-g -c -Wall -Wextra -pedantic
BIN=e8080

#--- Paths
SRC=$(E8080_PWD)/src
INCLUDE=$(E8080_PWD)/include

#--- Targets
.PHONY: clean tests run_tests e8080

e8080: $(OBJS) $(ENTRYPOINT)
	$(CC) -o $(BIN) $(OBJS) $(ENTRYPOINT)

tests: $(OBJS) $(TOBJS) $(TENTRYPOINT)
	$(CC) -o $(BIN)_test $(OBJS) $(TOBJS) $(TENTRYPOINT)
	make run_tests

run_tests: $(BIN)_test
	./$(BIN)_test

error.o: $(SRC)/error.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $<

cpu.o: $(SRC)/cpu.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $<

io.o: $(SRC)/io.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $<

disassembler.o: $(SRC)/disassembler.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $<

e8080.o: $(SRC)/e8080.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $(E8080_ENV) $<

disassembler_test.o: tests/disassembler_test.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $<

util_test.o: tests/util_test.c
	$(CC) $(CFLAGS) -I$(INCLUDE) $<

testing.o: tests/testing.c
	$(CC) $(CFLAGS) -std=gnu99 -I$(INCLUDE) $<

clean:
	@rm -f $(OBJS) $(TOBJS) $(ENTRYPOINT) $(TENTRYPOINT) $(BIN) $(BIN)_test *.log