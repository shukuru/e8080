/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file cpu.h
 * @brief Intel 8080 CPU main header file
*/

#ifndef E8080_CPU_HEADER
#define E8080_CPU_HEADER

/*
    INTEL 8080 processor brief specifications
    -----------------------------------------

    * Frequency:                2 to 4 MHz (2.000.000 to 4.000.000 fetch-decode-execute cycles per second)
    * Address BUS:              16 bits
    * Data BUS:                 8 bits
    * Registers:                8 bits
    * Stack pointer:            16 bits
    * Program counter:          16 bits
    * I/O:                      256 ports (0-255)
    * Endianness:               Big-endian
    
    The processor can address up to 64 kb of memory.
    
    Registers:
        * B (0):                General purpose register, can be accessed in pair with C register (MSB)
        * C (1):                General purpose register, can be accessed in pair with B register (LSB)
        * D (2):                General purpose register, can be accessed in pair with E register (MSB)
        * E (3):                General purpose register, can be accessed in pair with D register (LSB)
        * H (4):                Usually used to store MSB for memory addressing
        * L (5):                Usually used to store LSB for memory addressing
        * A (7):                Accumulator
    
    Execution time (cycle): Between 480ns and 2 µs
*/

#define E8080_CPU_REG_SIZE          sizeof(unsigned char)
#define E8080_CPU_ADDRESS_SIZE      sizeof(unsigned short)
#define E8080_CPU_LOW_ADDRESS       (unsigned char)0x00
#define E8080_CPU_HIGH_ADDRESS      (unsigned short)0xFFFF
#define E8080_CPU_SP_SIZE           sizeof(unsigned short)
#define E8080_CPU_PC_SIZE           sizeof(unsigned short)

#endif