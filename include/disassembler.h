/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file disassembler.h
 * @brief Intel 8080 disassembler header file
*/

#ifndef E8080_DISASSEMBLER_H
#define E8080_DISASSEMBLER_H

/* C standard headers */
#include <stdio.h>

/* e8080 headers */
#include "opcodes.h"
#include "cpu.h"
#include "util.h"
#include "error.h"

/**
 * @defgroup DisassemblerOpcodes e8080 disassembler opcodes macro-definitions
 * Groups all e8080 emulator opcodes mnemonics macro-definitions for it's disassembler
 * 
 * @{
*/
#define E8080_DISASSEMBLER_OPCODE_NOP   "NOP"
#define E8080_DISASSEMBLER_OPCODE_LXI   "LXI"
#define E8080_DISASSEMBLER_OPCODE_STAX  "STAX"
#define E8080_DISASSEMBLER_OPCODE_INX   "INX"
#define E8080_DISASSEMBLER_OPCODE_INR   "INR"
#define E8080_DISASSEMBLER_OPCODE_DCR   "DCR"
#define E8080_DISASSEMBLER_OPCODE_MVI   "MVI"
#define E8080_DISASSEMBLER_OPCODE_RLC   "RLC"
#define E8080_DISASSEMBLER_OPCODE_DAD   "DAD"
#define E8080_DISASSEMBLER_OPCODE_LDAX  "LDAX"
#define E8080_DISASSEMBLER_OPCODE_DCX   "DCX"
#define E8080_DISASSEMBLER_OPCODE_RRC   "RRC"
#define E8080_DISASSEMBLER_OPCODE_RAL   "RAL"
#define E8080_DISASSEMBLER_OPCODE_RAR   "RAR"
#define E8080_DISASSEMBLER_OPCODE_SHLD  "SHLD"
#define E8080_DISASSEMBLER_OPCODE_DAA   "DAA"
#define E8080_DISASSEMBLER_OPCODE_LHLD  "LHLD"
#define E8080_DISASSEMBLER_OPCODE_CMA   "CMA"
#define E8080_DISASSEMBLER_OPCODE_STA   "STA"
#define E8080_DISASSEMBLER_OPCODE_STC   "STC"
#define E8080_DISASSEMBLER_OPCODE_LDA   "LDA"
#define E8080_DISASSEMBLER_OPCODE_CMC   "CMC"
#define E8080_DISASSEMBLER_OPCODE_MOV   "MOV"
#define E8080_DISASSEMBLER_OPCODE_HLT   "HLT"
#define E8080_DISASSEMBLER_OPCODE_ADD   "ADD"
#define E8080_DISASSEMBLER_OPCODE_ADC   "ADC"
#define E8080_DISASSEMBLER_OPCODE_SUB   "SUB"
#define E8080_DISASSEMBLER_OPCODE_SSB   "SSB"
#define E8080_DISASSEMBLER_OPCODE_ANA   "ANA"
#define E8080_DISASSEMBLER_OPCODE_XRA   "XRA"
#define E8080_DISASSEMBLER_OPCODE_ORA   "ORA"
#define E8080_DISASSEMBLER_OPCODE_CMP   "CMP"
#define E8080_DISASSEMBLER_OPCODE_RNZ   "RNZ"
#define E8080_DISASSEMBLER_OPCODE_POP   "POP"
#define E8080_DISASSEMBLER_OPCODE_JNZ   "JNZ"
#define E8080_DISASSEMBLER_OPCODE_JMP   "JMP"
#define E8080_DISASSEMBLER_OPCODE_CNZ   "CNZ"
#define E8080_DISASSEMBLER_OPCODE_PUSH  "PUSH"
#define E8080_DISASSEMBLER_OPCODE_ADI   "ADI"
#define E8080_DISASSEMBLER_OPCODE_RST   "RST"
#define E8080_DISASSEMBLER_OPCODE_RZ    "RZ"
#define E8080_DISASSEMBLER_OPCODE_RET   "RET"
#define E8080_DISASSEMBLER_OPCODE_JZ    "JZ"
#define E8080_DISASSEMBLER_OPCODE_CZ    "CZ"
#define E8080_DISASSEMBLER_OPCODE_CALL  "CALL"
#define E8080_DISASSEMBLER_OPCODE_ACI   "ACI"
#define E8080_DISASSEMBLER_OPCODE_RNC   "RNC"
#define E8080_DISASSEMBLER_OPCODE_JNC   "JNC"
#define E8080_DISASSEMBLER_OPCODE_OUT   "OUT"
#define E8080_DISASSEMBLER_OPCODE_CNC   "CNC"
#define E8080_DISASSEMBLER_OPCODE_SUI   "SUI"
#define E8080_DISASSEMBLER_OPCODE_RC    "RC"
#define E8080_DISASSEMBLER_OPCODE_JC    "JC"
#define E8080_DISASSEMBLER_OPCODE_IN    "IN"
#define E8080_DISASSEMBLER_OPCODE_CC    "CC"
#define E8080_DISASSEMBLER_OPCODE_SBI   "SBI"
#define E8080_DISASSEMBLER_OPCODE_RPO   "RPO"
#define E8080_DISASSEMBLER_OPCODE_JPO   "JPO"
#define E8080_DISASSEMBLER_OPCODE_XTHL  "XTHL"
#define E8080_DISASSEMBLER_OPCODE_CPO   "CPO"
#define E8080_DISASSEMBLER_OPCODE_ANI   "ANI"
#define E8080_DISASSEMBLER_OPCODE_RPE   "RPE"
#define E8080_DISASSEMBLER_OPCODE_PCHL  "PCHL"
#define E8080_DISASSEMBLER_OPCODE_JPE   "JPE"
#define E8080_DISASSEMBLER_OPCODE_XCHG  "XCHG"
#define E8080_DISASSEMBLER_OPCODE_CPE   "CPE"
#define E8080_DISASSEMBLER_OPCODE_XRI   "XRI"
#define E8080_DISASSEMBLER_OPCODE_RP    "RP"
#define E8080_DISASSEMBLER_OPCODE_JP    "JP"
#define E8080_DISASSEMBLER_OPCODE_DI    "DI"
#define E8080_DISASSEMBLER_OPCODE_CP    "CP"
#define E8080_DISASSEMBLER_OPCODE_ORI   "ORI"
#define E8080_DISASSEMBLER_OPCODE_RM    "RM"
#define E8080_DISASSEMBLER_OPCODE_SPHL  "SPHL"
#define E8080_DISASSEMBLER_OPCODE_JM    "JM"
#define E8080_DISASSEMBLER_OPCODE_EI    "EI"
#define E8080_DISASSEMBLER_OPCODE_CM    "CM"
#define E8080_DISASSEMBLER_OPCODE_CPI   "CPI"
/**
 * @}
*/

/**
 * @def _P_OPCODE
 * @brief fprintf with stdout first parameter text shortener
*/
#define _P_OPCODE   fprintf(stdout,

/**
 * @defgroup DisassemblerSubroutines e8080 disassembler subroutines
 * Groups all e8080 emulator subroutines for it's disassembler
 * 
 * @{
*/
/**
 * @brief Disassemble an operation of 1, 2 or 3 bytes then display it to stdout
 * 
 * Displays the disassembled operation with given program counter position
 * 
 * @param opcode    1, 2 or 3 bytes buffer pointer to operation
 * @param pc        Stores program counter new value
 * 
 * @return Disassembled operation length
*/
unsigned char e8080_disassemble_opcode(unsigned char* opcode, unsigned short pc);

/**
 * @brief Disassemble Intel 8080 code from a fixed-size buffer.
 * 
 * Displays the disassembled code with given program counter position
 * 
 * @param buf        Fixed-size buffer to disassemble
 * @param buflen     Buffer length in bytes
 * 
 * @return Program counter current value
*/
unsigned short e8080_disassemble(unsigned char* buf, unsigned short buflen);
/**
 * @}
*/
#endif