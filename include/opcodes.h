/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file opcodes.h
 * @brief Intel 8080 CPU opcodes header file
*/

#ifndef E8080_OPCODES_HEADER
#define E8080_OPCODES_HEADER

/**
 * @defgroup INTEL_8080_OPCODES Intel 8080 OpCodes
 * Groups all CPU opcodes macro-definitions 
 * 
 * Opcodes are defined follwing this patern
 * 
 * \\#define E8080_<OPCODE>_<OPERAND 1>_<OPERAND 2>     <HEX CODE VALUE>
 * 
 * @note Opcodes 0x08, 0x10, 0x18, 0x20, 0x28, 0x30, 0x38, 0xcb, 0xdd, 0xed and 0xfd are alternatives
 * Opcodes and should not be used according Intel 8080 manual.
 * 
 * @{
*/

/**
 * @def E8080_NOP
 * @brief Intel 8080 NOP opcode
 * 
 * Doesn't compute anything, often used to pad some bytes in programs.
 * 
 * Program Counter (PC) is incremented by 1.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - -
*/
#define E8080_NOP                   (unsigned char)0x00

/**
 * @def E8080_LXI_B_D16
 * @brief Intel 8080 LXI opcode with BC register pairs and immediate 16 bit data operands
 * 
 * Stores 16 bits immediate data to BC register pairs.
 * MSB to B register, LSB to C register.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_LXI_B_D16             (unsigned char)0x01

/**
 * @def E8080_STAX_B
 * @brief Intel 8080 STAX opcode with BC register pairs operand
 * 
 * Stores accumulator (A) data at memory address specified by register pairs BC.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_STAX_B                (unsigned char)0x02

/**
 * @def E8080_INX_B
 * @brief Intel 8080 INX opcode with BC register pairs operand.
 * 
 * Increments data stored in BC register pairs by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_INX_B                 (unsigned char)0x03

/**
 * @def E8080_INR_B
 * @brief Intel 8080 INR opcode with B register operand
 * 
 * Increments data stored in B register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_B                 (unsigned char)0x04

/**
 * @def E8080_DCR_B
 * @brief Intel 8080 DCR opcode with B register operand
 * 
 * Decrements data in B register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_B                 (unsigned char)0x05

/**
 * @def E8080_MVI_B_D8
 * @brief Intel 8080 MVI opcode with B register operand
 * 
 * Stores the immediate 8 bits data to B register.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_B_D8              (unsigned char)0x06

/**
 * @def E8080_RLC
 * @brief Intel 8080 RLC opcode
 * 
 * Rotates "no carry" the accumulator (A) on the left.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - C
*/
#define E8080_RLC                   (unsigned char)0x07

/**
 * @def E8080_DAD_B
 * @brief Intel 8080 DAD opcode with BC register pairs operand
 * 
 * Add BC register pairs 16 bits stored value to HL register pairs one.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - C
*/
#define E8080_DAD_B                 (unsigned char)0x09

/**
 * @def E8080_LDAX_B
 * @brief Intel 8080 LDAX opcode with BC register pairs operand
 * 
 * Loads data from memory address specified by register pairs BC in accumulator (A).
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_LDAX_B                (unsigned char)0x0a

/**
 * @def E8080_DCX_B
 * @brief Intel 8080 DCX opcode with BC register pairs operand
 * 
 * Decrements data in BC register pairs by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_DCX_B                 (unsigned char)0x0b

/**
 * @def E8080_INR_C
 * @brief Intel 8080 INR opcode with C register operand
 * 
 * Increments data stored in C register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_C                 (unsigned char)0x0c

/**
 * @def E8080_DCR_C
 * @brief Intel 8080 DCR opcode with C register operand
 * 
 * Decrements data in C register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_C                 (unsigned char)0x0d

/**
 * @def E8080_MVI_C_D8
 * @brief Intel 8080 MVI opcode with C register operand
 * 
 * Stores the immediate 8 bits data to C register.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_C_D8              (unsigned char)0x0e

/**
 * @def E8080_RRC
 * @brief Intel 8080 RRC opcode
 * 
 * Rotates "no carry" the accumulator (A) on the right.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - C
*/
#define E8080_RRC                   (unsigned char)0x0f

/**
 * @def E8080_LXI_D_D16
 * @brief Intel 8080 LXI opcode with DE register pairs and immediate 16 bit data operands
 * 
 * Stores 16 bits immediate data to DE register pairs.
 * MSB to D register, LSB to E register.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_LXI_D_D16             (unsigned char)0x11

/**
 * @def E8080_STAX_D
 * @brief Intel 8080 STAX opcode with DE register pairs operand
 * 
 * Stores accumulator (A) data at memory address specified by register pairs DE.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_STAX_D                (unsigned char)0x12

/**
 * @def E8080_INX_D
 * @brief Intel 8080 INX opcode with DE register pairs operand
 * 
 * Increments data stored in DE register pairs by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_INX_D                 (unsigned char)0x13

/**
 * @def E8080_INR_D
 * @brief Intel 8080 INR opcode with D register operand
 * 
 * Increments data stored in D register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_D                 (unsigned char)0x14

/**
 * @def E8080_DCR_D
 * @brief Intel 8080 DCR opcode with D register operand
 * 
 * Decrements data in D register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_D                 (unsigned char)0x15

/**
 * @def E8080_MVI_D_D8
 * @brief Intel 8080 MVI opcode with D register operand
 * 
 * Stores the immediate 8 bits data to D register.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_D_D8              (unsigned char)0x16

/**
 * @def E8080_RAL
 * @brief Intel 8080 RAL opcode
 * 
 * Rotates "through carry" the accumulator (A) on the left.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - C
*/
#define E8080_RAL                   (unsigned char)0x17

/**
 * @def E8080_DAD_D
 * @brief Intel 8080 DAD opcode with DE register pairs operand
 * 
 * Add DE register pairs 16 bits stored value to HL register pairs one.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - C
*/
#define E8080_DAD_D                 (unsigned char)0x19

/**
 * @def E8080_LDAX_D
 * @brief Intel 8080 LDAX opcode with DE register pairs operand
 * 
 * Loads data from memory address specified by register pairs DE in accumulator (A).
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_LDAX_D                (unsigned char)0x1a

/**
 * @def E8080_DCX_D
 * @brief Intel 8080 DCX opcode with DE register pairs operand
 * 
 * Decrements data in DE register pairs by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_DCX_D                 (unsigned char)0x1b

/**
 * @def E8080_INR_E
 * @brief Intel 8080 INR opcode with E register operand
 * 
 * Increments data stored in E register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_E                 (unsigned char)0x1c

/**
 * @def E8080_DCR_E
 * @brief Intel 8080 DCR opcode with E register operand
 * 
 * Decrements data in E register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_E                 (unsigned char)0x1d

/**
 * @def E8080_MVI_E_D8
 * @brief Intel 8080 MVI opcode with E register operand
 * 
 * Stores the immediate 8 bits data to E register.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_E_D8              (unsigned char)0x1e

/**
 * @def E8080_RAR
 * @brief Intel 8080 RAR opcode
 * 
 * Rotates "through carry" the accumulator (A) on the right.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - C
*/
#define E8080_RAR                   (unsigned char)0x1f

/**
 * @def E8080_LXI_H_D16
 * @brief Intel 8080 LXI opcode with HL register pairs and immediate 16 bit data operands
 * 
 * Stores 16 bits immediate data to HL register pairs.
 * MSB to H register, LSB to L register.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_LXI_H_D16             (unsigned char)0x21

/**
 * @def E8080_SHLD_A16
 * @brief Intel 8080 SHLD opcode with 16 bits address operand
 * 
 * Stores L register data to ADDR operand and H register data to ADDR+1.
 * 
 * size:    3 bytes
 * time:    16 cycles
 * flags:   - - - - -
*/
#define E8080_SHLD_A16              (unsigned char)0x22


/**
 * @def E8080_INX_H
 * @brief Intel 8080 INX opcode with HL register pairs operand
 * 
 * Increments data stored in HL register pairs by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_INX_H                 (unsigned char)0x23

/**
 * @def E8080_INR_H
 * @brief Intel 8080 INR opcode with H register operand
 * 
 * Increments data stored in H register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_H                 (unsigned char)0x24

/**
 * @def E8080_DCR_H
 * @brief Intel 8080 DCR opcode with H register operand
 * 
 * Decrements data in H register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_H                 (unsigned char)0x25

/**
 * @def E8080_MVI_H_D8
 * @brief Intel 8080 MVI opcode with H register operand
 * 
 * Stores the immediate 8 bits data to H register.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_H_D8              (unsigned char)0x26

/**
 * @def E8080_DAA
 * @brief Intel 8080 DAA opcode
 * 
 * Performs a decimal addition of high and low nibble of accumulator (A) data.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_DAA                   (unsigned char)0x27

/**
 * @def E8080_DAD_H
 * @brief Intel 8080 DAD opcode with HL registers pair operand
 * 
 * Double 16 bits data stored in HL registers pair (HL << 1).
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - C
*/
#define E8080_DAD_H                 (unsigned char)0x29

/**
 * @def E8080_LHLD_A16
 * @brief Intel 8080 LHLD opcode with 16 bits address operand
 * 
 * Loads data from ADDR to L register and data from ADDR+1 to H register.
 * 
 * size:    3 bytes
 * time:    16 cycles
 * flags:   - - - - -
*/
#define E8080_LHLD_A16              (unsigned char)0x2a

/**
 * @def E8080_DCX_H
 * @brief Intel 8080 DCX opcode with HL register pairs operand
 * 
 * Decrements data in HL register pairs by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_DCX_H                 (unsigned char)0x2b

/**
 * @def E8080_INR_L
 * @brief Intel 8080 INR opcode with L register operand
 * 
 * Increments data stored in L register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_L                 (unsigned char)0x2c

/**
 * @def E8080_DCR_L
 * @brief Intel 8080 DCR opcode with L register operand
 * 
 * Decrements data in L register by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_L                 (unsigned char)0x2d

/**
 * @def E8080_MVI_L_D8
 * @brief Intel 8080 MVI opcode with L register operand
 * 
 * Stores the immediate 8 bits data to L register.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_L_D8              (unsigned char)0x2e

/**
 * @def E808_CMA
 * @brief Intel 8080 CMA opcode
 * 
 * Complements accumulator (A) 8 bits with one's complement -> A = !A.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - -
*/
#define E8080_CMA                   (unsigned char)0x2f

/**
 * @def E8080_LXI_SP_D16
 * @brief Intel 8080 LXI opcode with stack pointer (SP) and immediate 16 bit data operands
 * 
 * Stores 16 bits immediate data to stack pointer (SP).
 * The MSB of d16 will be stored in the SP low nibble and the LSB of d16 will be stored in SP high nibble.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_LXI_SP_D16             (unsigned char)0x31

/**
 * @def E8080_STA_A16
 * @brief Intel 8080 STA opcode with 16 bits memory address operand
 * 
 * Stores accumulator (A) to specified 16 bits address by the operand.
 * 
 * size:    3 bytes
 * time:    13 cycles
 * flags:   - - - - -
*/
#define E8080_STA_A16               (unsigned char)0x32

/**
 * @def E8080_INX_SP
 * @brief Intel 8080 INX opcode with stack pointer (SP) operand
 * 
 * Increments data stored in stack pointer (SP) by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_INX_SP                (unsigned char)0x33

/**
 * @def E8080_INR_M
 * @brief Intel 8080 INR opcode with indirect addressing
 * 
 * Increments stored data located at memory address stored in HL registers pair by 1.
 * If stack pointer contains 0xFFFF, this operation will turn it into 0x0000.
 * 
 * size:    1 byte
 * times:   10 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_M                 (unsigned char)0x34

/**
 * @def E8080_DCR_M
 * @brief Intel 8080 DCR opcode with indirect addressing
 * 
 * Decrements stored data located at memory address stored in HL registers pair by 1.
 * 
 * size:    1 byte
 * times:   10 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_M                 (unsigned char)0x35

/**
 * @def E8080_MVI_M_D8
 * @brief Intel 8080 MVI opcode with indirect addressing
 * 
 * Stores the immediate 8 bits data at memory address stored in HL registers pair.
 * 
 * size:    2 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_M_D8              (unsigned char)0x36

/**
 * @def E8080_STC
 * @brief Intel 8080 STC opcode
 * 
 * Set the CARRY bit in flags register (F) to 1.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - C
*/
#define E8080_STC                   (unsigned char)0x37

/**
 * @def E8080_DAD_SP
 * @brief Intel 8080 DAD opcode with stack pointer (SP) operand
 * 
 * Add 16 bits stored value to stack pointer (SP) to HL register pairs one.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - C
*/
#define E8080_DAD_SP                (unsigned char)0x39

/**
 * @def E8080_LDA_A16
 * @brief Intel 8080 LDA opcode with 16 bits memory address operand
 * 
 * Loads the data stored at 16 bits memory address operand in accumulator (A).
 * 
 * size:    3 bytes
 * time:    13 cycles
 * flags:   - - - - -
*/
#define E8080_LDA_A16               (unsigned char)0x3a

/**
 * @def E8080_DCX_SP
 * @brief Intel 8080 DCX opcode with stack pointer (SP) operand
 * 
 * Decrements data stored in stack pointer (SP) by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_DCX_SP                (unsigned char)0x3b

/**
 * @def E8080_INR_A
 * @brief Intel 8080 INR opcode with accumulator (A) operand
 * 
 * Increments data stored in accumulator (A) by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_INR_A                 (unsigned char)0x3c

/**
 * @def E8080_DCR_A
 * @brief Intel 8080 DCR opcode with accumulator (A) operand
 * 
 * Decrements data stored in accumulator (A) by 1.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   S Z A P -
*/
#define E8080_DCR_A                 (unsigned char)0x3d

/**
 * @def E8080_MVI_A_D8
 * @brief Intel 8080 MVI opcode with accumulator (A) operand
 * 
 * Stores the immediate 8 bits data to accumulator (A).
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MVI_A_D8              (unsigned char)0x3e

/**
 * @def E8080_CMC
 * @brief Intel 8080 CMC opcode
 * 
 * Complements carry bit with one's complement -> CARRY = !CARRY.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - C
*/
#define E8080_CMC                   (unsigned char)0x3f

/**
 * @def E8080_MOV_B_B
 * @brief Intel 8080 MOV opcode with B register as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_B               (unsigned char)0x40

/**
 * @def E8080_MOV_B_C
 * @brief Intel 8080 MOV opcode with B register as destination operand and C register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_C               (unsigned char)0x41

/**
 * @def E8080_MOV_B_D
 * @brief Intel 8080 MOV opcode with B register as destination operand and D register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_D               (unsigned char)0x42

/**
 * @def E8080_MOV_B_E
 * @brief Intel 8080 MOV opcode with B register as destination operand and E register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_E               (unsigned char)0x43

/**
 * @def E8080_MOV_B_H
 * @brief Intel 8080 MOV opcode with B register as destination operand and H register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_H               (unsigned char)0x44

/**
 * @def E8080_MOV_B_L
 * @brief Intel 8080 MOV opcode with B register as destination operand and L register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_L               (unsigned char)0x45

/**
 * @def E8080_MOV_B_M
 * @brief Intel 8080 MOV opcode with B register as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_M               (unsigned char)0x46

/**
 * @def E8080_MOV_B_A
 * @brief Intel 8080 MOV opcode with B register as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_B_A               (unsigned char)0x47

/**
 * @def E8080_MOV_C_B
 * @brief Intel 8080 MOV opcode with C register as destination operand and B register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_B               (unsigned char)0x48

/**
 * @def E8080_MOV_C_C
 * @brief Intel 8080 MOV opcode with C register as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_C               (unsigned char)0x49

/**
 * @def E8080_MOV_C_D
 * @brief Intel 8080 MOV opcode with C register as destination operand and D register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_D               (unsigned char)0x4a

/**
 * @def E8080_MOV_C_E
 * @brief Intel 8080 MOV opcode with C register as destination operand and E register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_E               (unsigned char)0x4b

/**
 * @def E8080_MOV_C_H
 * @brief Intel 8080 MOV opcode with C register as destination operand and H register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_H               (unsigned char)0x4c

/**
 * @def E8080_MOV_C_L
 * @brief Intel 8080 MOV opcode with C register as destination operand and L register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_L               (unsigned char)0x4d

/**
 * @def E8080_MOV_C_M
 * @brief Intel 8080 MOV opcode with C register as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_M               (unsigned char)0x4e

/**
 * @def E8080_MOV_C_A
 * @brief Intel 8080 MOV opcode with C register as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_C_A               (unsigned char)0x4f

/**
 * @def E8080_MOV_D_B
 * @brief Intel 8080 MOV opcode with D register as destination operand and B register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_B               (unsigned char)0x50

/**
 * @def E8080_MOV_D_C
 * @brief Intel 8080 MOV opcode with D register as destination operand and C register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_C               (unsigned char)0x51

/**
 * @def E8080_MOV_D_D
 * @brief Intel 8080 MOV opcode with D register as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_D               (unsigned char)0x52

/**
 * @def E8080_MOV_D_E
 * @brief Intel 8080 MOV opcode with D register as destination operand and E register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_E               (unsigned char)0x53

/**
 * @def E8080_MOV_D_H
 * @brief Intel 8080 MOV opcode with D register as destination operand and H register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_H               (unsigned char)0x54

/**
 * @def E8080_MOV_D_L
 * @brief Intel 8080 MOV opcode with D register as destination operand and L register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_L               (unsigned char)0x55

/**
 * @def E8080_MOV_D_M
 * @brief Intel 8080 MOV opcode with D register as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_M               (unsigned char)0x56

/**
 * @def E8080_MOV_D_A
 * @brief Intel 8080 MOV opcode with D register as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_D_A               (unsigned char)0x57

/**
 * @def E8080_MOV_E_B
 * @brief Intel 8080 MOV opcode with E register as destination operand and B register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_B               (unsigned char)0x58

/**
 * @def E8080_MOV_E_C
 * @brief Intel 8080 MOV opcode with E register as destination operand and C register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_C               (unsigned char)0x59

/**
 * @def E8080_MOV_E_D
 * @brief Intel 8080 MOV opcode with E register as destination operand and D register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_D               (unsigned char)0x5a

/**
 * @def E8080_MOV_E_E
 * @brief Intel 8080 MOV opcode with E register as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_E               (unsigned char)0x5b

/**
 * @def E8080_MOV_E_H
 * @brief Intel 8080 MOV opcode with E register as destination operand and H register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_H               (unsigned char)0x5c

/**
 * @def E8080_MOV_E_L
 * @brief Intel 8080 MOV opcode with E register as destination operand and L register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_L               (unsigned char)0x5d

/**
 * @def E8080_MOV_E_M
 * @brief Intel 8080 MOV opcode with E register as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_M               (unsigned char)0x5e

/**
 * @def E8080_MOV_E_A
 * @brief Intel 8080 MOV opcode with E register as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_E_A               (unsigned char)0x5f

/**
 * @def E8080_MOV_H_B
 * @brief Intel 8080 MOV opcode with H register as destination operand and B register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_B               (unsigned char)0x60

/**
 * @def E8080_MOV_H_C
 * @brief Intel 8080 MOV opcode with H register as destination operand and C register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_C               (unsigned char)0x61

/**
 * @def E8080_MOV_H_D
 * @brief Intel 8080 MOV opcode with H register as destination operand and D register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_D               (unsigned char)0x62

/**
 * @def E8080_MOV_H_E
 * @brief Intel 8080 MOV opcode with H register as destination operand and E register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_E               (unsigned char)0x63

/**
 * @def E8080_MOV_H_H
 * @brief Intel 8080 MOV opcode with H register as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_H               (unsigned char)0x64

/**
 * @def E8080_MOV_H_L
 * @brief Intel 8080 MOV opcode with H register as destination operand and L register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_L               (unsigned char)0x65

/**
 * @def E8080_MOV_H_M
 * @brief Intel 8080 MOV opcode with H register as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the first one.
 * The H register will be affected since it is used to stores high byte of a 16 bits address.
 * However, the data stored at previous specified address remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_M               (unsigned char)0x66

/**
 * @def E8080_MOV_H_A
 * @brief Intel 8080 MOV opcode with H register as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_H_A               (unsigned char)0x67

/**
 * @def E8080_MOV_L_B
 * @brief Intel 8080 MOV opcode with L register as destination operand and B register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_B               (unsigned char)0x68

/**
 * @def E8080_MOV_L_C
 * @brief Intel 8080 MOV opcode with L register as destination operand and C register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_C               (unsigned char)0x69

/**
 * @def E8080_MOV_L_D
 * @brief Intel 8080 MOV opcode with L register as destination operand and D register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_D               (unsigned char)0x6a

/**
 * @def E8080_MOV_L_E
 * @brief Intel 8080 MOV opcode with L register as destination operand and E register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_E               (unsigned char)0x6b

/**
 * @def E8080_MOV_L_H
 * @brief Intel 8080 MOV opcode with L register as destination operand and H register as source operand
 * 
 * Copy data from second register operand in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_H               (unsigned char)0x6c

/**
 * @def E8080_MOV_L_L
 * @brief Intel 8080 MOV opcode with L register as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_L               (unsigned char)0x6d

/**
 * @def E8080_MOV_L_M
 * @brief Intel 8080 MOV opcode with L register as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the first one.
 * The L register will be affected since it is used to stores low byte of a 16 bits address.
 * However, the data stored at previous specified address remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_M               (unsigned char)0x6e

/**
 * @def E8080_MOV_L_A
 * @brief Intel 8080 MOV opcode with L register as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) in the first one.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_L_A               (unsigned char)0x6f

/**
 * @def E8080_MOV_M_B
 * @brief Intel 8080 MOV opcode with memory location as destination operand and B register as source operand
 * 
 * Copy data from second operand at specified memory address by HL registers pair.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_B               (unsigned char)0x70

/**
 * @def E8080_MOV_M_C
 * @brief Intel 8080 MOV opcode with memory location as destination operand and C register as source operand
 * 
 * Copy data from second operand at specified memory address by HL registers pair.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_C               (unsigned char)0x71

/**
 * @def E8080_MOV_M_D
 * @brief Intel 8080 MOV opcode with memory location as destination operand and D register as source operand
 * 
 * Copy data from second operand at specified memory address by HL registers pair.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_D               (unsigned char)0x72

/**
 * @def E8080_MOV_M_E
 * @brief Intel 8080 MOV opcode with memory location as destination operand and E register as source operand
 * 
 * Copy data from second operand at specified memory address by HL registers pair.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_E               (unsigned char)0x73

/**
 * @def E8080_MOV_M_H
 * @brief Intel 8080 MOV opcode with memory location as destination operand and H register as source operand
 * 
 * Copy data from second operand at specified memory address by HL registers pair.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_H               (unsigned char)0x74

/**
 * @def E8080_MOV_M_L
 * @brief Intel 8080 MOV opcode with memory location as destination operand and L register as source operand
 * 
 * Copy data from second operand at specified memory address by HL registers pair.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_L               (unsigned char)0x75

/**
 * @def E8080_HLT
 * @brief Intel 8080 HLT opcode
 * 
 * Increments the program counter (PC) to the next instruction and enters the STOPPED state
 * until an interruption occurs.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_HLT                   (unsigned char)0x76

/**
 * @def E8080_MOV_M_A
 * @brief Intel 8080 MOV opcode with memory location as destination operand and accumulator (A) as source operand
 * 
 * Copy data from accumulator (A) at specified memory address by HL registers pair
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_M_A               (unsigned char)0x77

/**
 * @def E8080_MOV_A_B
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand and B register as source operand
 * 
 * Copy data from second register operand in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_B               (unsigned char)0x78

/**
 * @def E8080_MOV_A_C
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand and C register as source operand
 * 
 * Copy data from second register operand in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_C               (unsigned char)0x79

/**
 * @def E8080_MOV_A_D
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand and D register as source operand
 * 
 * Copy data from second register operand in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_D               (unsigned char)0x7a

/**
 * @def E8080_MOV_A_E
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand and E register as source operand
 * 
 * Copy data from second register operand in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_E               (unsigned char)0x7b

/**
 * @def E8080_MOV_A_H
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand and H register as source operand
 * 
 * Copy data from second register operand in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_H               (unsigned char)0x7c

/**
 * @def E8080_MOV_A_L
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand and L register as source operand
 * 
 * Copy data from second register operand in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_L               (unsigned char)0x7d

/**
 * @def E8080_MOV_A_M
 * @brief Intel 8080 MOV opcode with accumulator (A) as destination operand with indirect addressing source operand
 * 
 * Copy data from memory address specified by HL registers pair in the accumulator (A).
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_M               (unsigned char)0x7e

/**
 * @def E8080_MOV_A_A
 * @brief Intel 8080 MOV opcode with accumulator (A) as two operands
 * 
 * Copy data from second register operand in the first one.
 * As the two operand are the same register, this is a null operation such as NOP.
 * The source operand remains unchanged.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_MOV_A_A               (unsigned char)0x7f

/**
 * @def E8080_ADD_B
 * @brief Intel 8080 ADD opcode with B register as operand
 * 
 * Add the data stored in B register with accumulator (A) data and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_B                 (unsigned char)0x80

/**
 * @def E8080_ADD_C
 * @brief Intel 8080 ADD opcode with C register as operand
 * 
 * Add the data stored in C register with accumulator (A) data and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_C                 (unsigned char)0x81

/**
 * @def E8080_ADD_D
 * @brief Intel 8080 ADD opcode with D register as operand
 * 
 * Add the data stored in D register with accumulator (A) data and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_D                 (unsigned char)0x82

/**
 * @def E8080_ADD_E
 * @brief Intel 8080 ADD opcode with E register as operand
 * 
 * Add the data stored in E register with accumulator (A) data and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_E                 (unsigned char)0x83

/**
 * @def E8080_ADD_H
 * @brief Intel 8080 ADD opcode with H register as operand
 * 
 * Add the data stored in H register with accumulator (A) data and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_H                 (unsigned char)0x84

/**
 * @def E8080_ADD_L
 * @brief Intel 8080 ADD opcode with L register as operand
 * 
 * Add the data stored in L register with accumulator (A) data and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_L                 (unsigned char)0x85

/**
 * @def E8080_ADD_M
 * @brief Intel 8080 ADD opcode with indirect memory addressing operand
 * 
 * Add the data stored at memory location specified by HL registers pair with accumulator (A) data and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_M                 (unsigned char)0x86

/**
 * @def E8080_ADD_A
 * @brief Intel 8080 ADD opcode with accumulator (A) as operand
 * 
 * Doubles the accumulator (A) stored data value.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADD_A                 (unsigned char)0x87

/**
 * @def E8080_ADC_B
 * @brief Intel 8080 ADC opcode with B register as operand
 * 
 * Add the data stored in B register with accumulator (A) data plus the carry bit value 
 * and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_B                 (unsigned char)0x88

/**
 * @def E8080_ADC_C
 * @brief Intel 8080 ADC opcode with C register as operand
 * 
 * Add the data stored in C register with accumulator (A) data plus the carry bit value 
 * and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_C                 (unsigned char)0x89

/**
 * @def E8080_ADC_D
 * @brief Intel 8080 ADC opcode with D register as operand
 * 
 * Add the data stored in D register with accumulator (A) data plus the carry bit value 
 * and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_D                 (unsigned char)0x8a

/**
 * @def E8080_ADC_E
 * @brief Intel 8080 ADC opcode with E register as operand
 * 
 * Add the data stored in E register with accumulator (A) data plus the carry bit value 
 * and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_E                 (unsigned char)0x8b

/**
 * @def E8080_ADC_H
 * @brief Intel 8080 ADC opcode with H register as operand
 * 
 * Add the data stored in H register with accumulator (A) data plus the carry bit value 
 * and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_H                 (unsigned char)0x8c

/**
 * @def E8080_ADC_L
 * @brief Intel 8080 ADC opcode with L register as operand
 * 
 * Add the data stored in L register with accumulator (A) data plus the carry bit value 
 * and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_L                 (unsigned char)0x8d

/**
 * @def E8080_ADC_M
 * @brief Intel 8080 ADC opcode with indirect memory addressing operand
 * 
 * Add the data stored at memory location specified by HL registers pair with accumulator (A) data
 * plus the carry bit value and stores the result in it.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_M                 (unsigned char)0x8e

/**
 * @def E8080_ADC_A
 * @brief Intel 8080 ADC opcode with accumulator (A) as operand
 * 
 * Doubles the accumulator (A) stored data value plus the carry bit value.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ADC_A                 (unsigned char)0x8f

/**
 * @def E8080_SUB_B
 * @brief Intel 8080 SUB opcode with B register as operand
 * 
 * Substracts the data stored in B register from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_B                 (unsigned char)0x90

/**
 * @def E8080_SUB_C
 * @brief Intel 8080 SUB opcode with C register as operand
 * 
 * Substracts the data stored in C register from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_C                 (unsigned char)0x91

/**
 * @def E8080_SUB_D
 * @brief Intel 8080 SUB opcode with D register as operand
 * 
 * Substracts the data stored in D register from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_D                 (unsigned char)0x92

/**
 * @def E8080_SUB_E
 * @brief Intel 8080 SUB opcode with E register as operand
 * 
 * Substracts the data stored in E register from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_E                 (unsigned char)0x93

/**
 * @def E8080_SUB_H
 * @brief Intel 8080 SUB opcode with H register as operand
 * 
 * Substracts the data stored in H register from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_H                 (unsigned char)0x94

/**
 * @def E8080_SUB_L
 * @brief Intel 8080 SUB opcode with L register as operand
 * 
 * Substracts the data stored in L register from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_L                 (unsigned char)0x95

/**
 * @def E8080_SUB_M
 * @brief Intel 8080 SUB opcode with indirect memory addressing as operand
 * 
 * Substracts the data stored from memory address specified by HL registers pair from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_M                 (unsigned char)0x96

/**
 * @def E8080_SUB_A
 * @brief Intel 8080 SUB opcode with accumulator (A) as operand
 * 
 * Substracts the accumulator (A) from itself.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SUB_A                 (unsigned char)0x97

/**
 * @def E8080_SSB_B
 * @brief Intel 8080 SSB opcode with B register as operand
 * 
 * Substracts the data stored in B register, with borrow bit added, from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_B                 (unsigned char)0x98

/**
 * @def E8080_SSB_C
 * @brief Intel 8080 SSB opcode with C register as operand
 * 
 * Substracts the data stored in C register, with borrow bit added, from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_C                 (unsigned char)0x99

/**
 * @def E8080_SSB_D
 * @brief Intel 8080 SSB opcode with D register as operand
 * 
 * Substracts the data stored in D register, with borrow bit added, from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_D                 (unsigned char)0x9a

/**
 * @def E8080_SSB_E
 * @brief Intel 8080 SSB opcode with E register as operand
 * 
 * Substracts the data stored in E register, with borrow bit added, from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_E                 (unsigned char)0x9b

/**
 * @def E8080_SSB_H
 * @brief Intel 8080 SSB opcode with H register as operand
 * 
 * Substracts the data stored in H register, with borrow bit added, from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_H                 (unsigned char)0x9c

/**
 * @def E8080_SSB_L
 * @brief Intel 8080 SSB opcode with L register as operand
 * 
 * Substracts the data stored in L register, with borrow bit added, from accumulator (A) and 
 * stores the result in it.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_L                 (unsigned char)0x9d

/**
 * @def E8080_SSB_M
 * @brief Intel 8080 SSB opcode with indirect memory addressing as operand
 * 
 * Substracts the data stored from memory address specified by HL registers pair, with borrow bit added, 
 * from accumulator (A) and stores the result in it.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_M                 (unsigned char)0x9e

/**
 * @def E8080_SSB_A
 * @brief Intel 8080 SSB opcode with accumulator (A) as operand
 * 
 * Substracts the accumulator (A) from itself with borrow bit added.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_SSB_A                 (unsigned char)0x9f

/**
 * @def E8080_ANA_B
 * @brief Intel 8080 ANA opcode with B register as operand
 * 
 * AND accumulator (A) with B register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_B                 (unsigned char)0xa0

/**
 * @def E8080_ANA_C
 * @brief Intel 8080 ANA opcode with C register as operand
 * 
 * AND accumulator (A) with C register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_C                 (unsigned char)0xa1

/**
 * @def E8080_ANA_D
 * @brief Intel 8080 ANA opcode with D register as operand
 * 
 * AND accumulator (A) with D register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_D                 (unsigned char)0xa2

/**
 * @def E8080_ANA_E
 * @brief Intel 8080 ANA opcode with E register as operand
 * 
 * AND accumulator (A) with E register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_E                 (unsigned char)0xa3

/**
 * @def E8080_ANA_H
 * @brief Intel 8080 ANA opcode with H register as operand
 * 
 * AND accumulator (A) with H register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_H                 (unsigned char)0xa4

/**
 * @def E8080_ANA_L
 * @brief Intel 8080 ANA opcode with L register as operand
 * 
 * AND accumulator (A) with L register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_L                 (unsigned char)0xa5

/**
 * @def E8080_ANA_M
 * @brief Intel 8080 ANA opcode with indirect addressing mode as operand
 * 
 * AND accumulator (A) with value stored at memory address specified by HL registers pair, 
 * result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_M                 (unsigned char)0xa6

/**
 * @def E8080_ANA_A
 * @brief Intel 8080 ANA opcode with accumulator (A) register as operand
 * 
 * AND accumulator (A) with itself, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ANA_A                 (unsigned char)0xa7

/**
 * @def E8080_XRA_B
 * @brief Intel 8080 XRA opcode with B register as operand
 * 
 * XOR accumulator (A) with B register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_B                 (unsigned char)0xa8

/**
 * @def E8080_XRA_C
 * @brief Intel 8080 XRA opcode with C register as operand
 * 
 * XOR accumulator (A) with C register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_C                 (unsigned char)0xa9

/**
 * @def E8080_XRA_D
 * @brief Intel 8080 XRA opcode with D register as operand
 * 
 * XOR accumulator (A) with D register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_D                 (unsigned char)0xaa

/**
 * @def E8080_XRA_E
 * @brief Intel 8080 XRA opcode with E register as operand
 * 
 * XOR accumulator (A) with E register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_E                 (unsigned char)0xab

/**
 * @def E8080_XRA_H
 * @brief Intel 8080 XRA opcode with H register as operand
 * 
 * XOR accumulator (A) with H register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_H                 (unsigned char)0xac

/**
 * @def E8080_XRA_L
 * @brief Intel 8080 XRA opcode with L register as operand
 * 
 * XOR accumulator (A) with L register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_L                 (unsigned char)0xad

/**
 * @def E8080_XRA_M
 * @brief Intel 8080 XRA opcode with indirect addressing mode as operand
 * 
 * XOR accumulator (A) with value stored at memory address specified by HL registers pair, 
 * result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_M                 (unsigned char)0xae

/**
 * @def E8080_XRA_A
 * @brief Intel 8080 XRA opcode with accumulator (A) register as operand
 * 
 * XOR accumulator (A) with itself, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_XRA_A                 (unsigned char)0xaf

/**
 * @def E8080_ORA_B
 * @brief Intel 8080 ORA opcode with B register as operand
 * 
 * OR accumulator (A) with B register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_B                 (unsigned char)0xb0

/**
 * @def E8080_ORA_C
 * @brief Intel 8080 ORA opcode with C register as operand
 * 
 * OR accumulator (A) with C register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_C                 (unsigned char)0xb1

/**
 * @def E8080_ORA_D
 * @brief Intel 8080 ORA opcode with D register as operand
 * 
 * OR accumulator (A) with D register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_D                 (unsigned char)0xb2

/**
 * @def E8080_ORA_E
 * @brief Intel 8080 ORA opcode with E register as operand
 * 
 * OR accumulator (A) with E register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_E                 (unsigned char)0xb3

/**
 * @def E8080_ORA_H
 * @brief Intel 8080 ORA opcode with H register as operand
 * 
 * OR accumulator (A) with H register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_H                 (unsigned char)0xb4

/**
 * @def E8080_ORA_L
 * @brief Intel 8080 ORA opcode with L register as operand
 * 
 * OR accumulator (A) with L register, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_L                 (unsigned char)0xb5

/**
 * @def E8080_ORA_M
 * @brief Intel 8080 ORA opcode with indirect addressing mode as operand
 * 
 * OR accumulator (A) with value stored at memory address specified by HL registers pair, 
 * result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_M                 (unsigned char)0xb6

/**
 * @def E8080_ORA_A
 * @brief Intel 8080 ORA opcode with accumulator (A) as operand
 * 
 * OR accumulator (A) with itself, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_ORA_A                 (unsigned char)0xb7

/**
 * @def E8080_CMP_B
 * @brief Intel 8080 CMP opcode with B register as operand
 * 
 * Compares B register value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_B                 (unsigned char)0xb8

/**
 * @def E8080_CMP_C
 * @brief Intel 8080 CMP opcode with C register as operand
 * 
 * Compares C register value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_C                 (unsigned char)0xb9

/**
 * @def E8080_CMP_D
 * @brief Intel 8080 CMP opcode with D register as operand
 * 
 * Compares D register value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_D                 (unsigned char)0xba

/**
 * @def E8080_CMP_E
 * @brief Intel 8080 CMP opcode with E register as operand
 * 
 * Compares E register value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_E                 (unsigned char)0xbb

/**
 * @def E8080_CMP_H
 * @brief Intel 8080 CMP opcode with H register as operand
 * 
 * Compares H register value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_H                 (unsigned char)0xbc

/**
 * @def E8080_CMP_L
 * @brief Intel 8080 CMP opcode with L register as operand
 * 
 * Compares L register value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_L                 (unsigned char)0xbd

/**
 * @def E8080_CMP_M
 * @brief Intel 8080 CMP opcode with indirect addressing mode as operand
 * 
 * Compares stored value at memory address sepcified by HL registers pair with accumulator (A) one 
 * then set the Zero bit flag if equality is true.
 * 
 * Both values remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_M                 (unsigned char)0xbe

/**
 * @def E8080_CMP_A
 * @brief Intel 8080 CMP opcode with accumulator (A) register as operand
 * 
 * Compares accumulator (A) with itself. Since this operation is an internal result
 * of a substraction, the result will be always true and the Zero bit flag will always
 * be set.
 * 
 * The accumulator (A) value remain unchanged.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CMP_A                 (unsigned char)0xbf

/**
 * @def E8080_RNZ
 * @brief Intel 8080 RNZ opcode
 * 
 * Performs a return operation if the Zero bit flag (Z) is not set.
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RNZ                   (unsigned char)0xc0

/**
 * @def E8080_POP_B
 * @brief Intel 8080 POP opcode with BC registers pair as operand
 * 
 * Pop data off the stack in BC registers pair from the specified memory address by the stack pointer (SP).
 * The 8-bit data from SP is stored in the second register, the 8-bit data from SP+1 is stored in the
 * first register.
 * 
 * This operation will increment the stack pointer value by 2.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_POP_B                 (unsigned char)0xc1

/**
 * @def E8080_JNZ_A16
 * @brief Intel 8080 JNZ opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Zero bit flag (Z) is not set.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JNZ_A16               (unsigned char)0xc2

/**
 * @def E8080_JMP_A16
 * @brief Intel 8080 JMP opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JMP_A16               (unsigned char)0xc3

/**
 * @def E8080_CNZ_A16
 * @brief Intel 8080 CNZ opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Zero bit flag (Z) is not set.
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CNZ_A16               (unsigned char)0xc4

/**
 * @def E8080_PUSH_B
 * @brief Intel 8080 PUSH opcode with BC registers pair as operand
 * 
 * Push BC registers pair data on stack at memory address specified by stack pointer (SP).
 * The second register data will be pushed at SP-1 and the first register data pushed to SP-2.
 * 
 * This operation will decrement the stack pointer by 2.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - - 
*/
#define E8080_PUSH_B                (unsigned char)0xc5

/**
 * @def E8080_ADI_D8
 * @brief Intel 8080 ADI opcode with immediate 8 bits data operand
 * 
 * Add immediate 8 bit operand value to the accumulator (A) data using two's complement.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ADI_D8                (unsigned char)0xc6

/**
 * @def E8080_RST_0
 * @brief Intel 8080 RST opcode with special subroutine 0 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_0                 (unsigned char)0xc7

/**
 * @def E8080_RZ
 * @brief Intel 8080 RZ opcode
 * 
 * Performs a return operation if the Zero bit flag (Z) is set.
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RZ                   (unsigned char)0xc8

/**
 * @def E8080_RET
 * @brief Intel 8080 RET opcode
 * 
 * Performs a return operation at the memory address specified by program counter (PC).
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_RET                   (unsigned char)0xc9

/**
 * @def E8080_JZ_A16
 * @brief Intel 8080 JZ opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Zero bit flag (Z) is set.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JZ_A16               (unsigned char)0xca

/**
 * @def E8080_CZ_A16
 * @brief Intel 8080 CZ opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Zero bit flag (Z) is set.
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CZ_A16               (unsigned char)0xcc

/**
 * @def E8080_CALL_A16
 * @brief Intel 8080 CALL opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address spedified by operand.
 * 
 * size:    3 bytes
 * time:    17 cycles
 * flags:   - - - - -
*/
#define E8080_CALL_A16             (unsigned char)0xcd

/**
 * @def E8080_ACI_D8
 * @brief Intel 8080 ACI opcode with immediate 8 bits data operand
 * 
 * Add immediate 8 bit operand value to the accumulator (A) data plus the carry bit using two's complement.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ACI_D8                (unsigned char)0xce

/**
 * @def E8080_RST_1
 * @brief Intel 8080 RST opcode with special subroutine 1 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_1                 (unsigned char)0xcf

/**
 * @def E8080_RNC
 * @brief Intel 8080 RNC opcode
 * 
 * Performs a return operation at the memory address specified by program counter (PC)
 * if the carry bit (CY) is not set.
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RNC                   (unsigned char)0xd0

/**
 * @def E8080_POP_D
 * @brief Intel 8080 POP opcode with DE registers pair as operand
 * 
 * Pop data off the stack in DE registers pair from the specified memory address by the stack pointer (SP).
 * The 8-bit data from SP is stored in the second register, the 8-bit data from SP+1 is stored in the
 * first register.
 * 
 * This operation will increment the stack pointer value by 2.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_POP_D                 (unsigned char)0xd1

/**
 * @def E8080_JNC_A16
 * @brief Intel 8080 JNC opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Carry bit flag (CY) is not set.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JNC_A16               (unsigned char)0xd2

/**
 * @def E8080_OUT_D8
 * @brief Intel 8080 OUT opcode with 8 bits immediate data operand
 * 
 * This operation send the content of the accumulator (A) to the device port number
 * specified by immediate 8 bits value operand.
 * 
 * size:    2 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_OUT_D8                (unsigned char)0xd3

/**
 * @def E8080_CNC_A16
 * @brief Intel 8080 CNC opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Carry bit flag (CY) is not set.
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CNC_A16               (unsigned char)0xd4

/**
 * @def E8080_PUSH_D
 * @brief Intel 8080 PUSH opcode with DE registers pair as operand
 * 
 * Push DE registers pair data on stack at memory address specified by stack pointer (SP).
 * The second register data will be pushed at SP-1 and the first register data pushed to SP-2.
 * 
 * This operation will decrement the stack pointer by 2.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - - 
*/
#define E8080_PUSH_D                (unsigned char)0xd5

/**
 * @def E8080_SUI_D8
 * @brief Intel 8080 SUI opcode with immediate 8 bits data operand
 * 
 * Substracts immediate 8 bit operand value to the accumulator (A) data using two's complement.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_SUI_D8                (unsigned char)0xd6

/**
 * @def E8080_RST_2
 * @brief Intel 8080 RST opcode with special subroutine 2 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_2                 (unsigned char)0xd7

/**
 * @def E8080_RC
 * @brief Intel 8080 RC opcode
 * 
 * Performs a return operation at the memory address specified by program counter (PC)
 * if the carry bit (CY) is set.
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RC                   (unsigned char)0xd8

/**
 * @def E8080_JC_A16
 * @brief Intel 8080 JC opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Carry bit flag (CY) is set.
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JC_A16               (unsigned char)0xda

/**
 * @def E8080_IN_D8
 * @brief Intel 8080 IN opcode with 8 bits immediate data operand
 * 
 * This operation reads the content of the accumulator (A) from the device port number
 * specified by immediate 8 bits value operand.
 * 
 * size:    2 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_IN_D8                (unsigned char)0xdb

/**
 * @def E8080_CC_A16
 * @brief Intel 8080 CC opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Carry bit flag (CY) is set.
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CC_A16               (unsigned char)0xdc

/**
 * @def E8080_SBI_D8
 * @brief Intel 8080 SBI opcode with immediate 8 bits data operand
 * 
 * Substracts immediate 8 bit operand value minus the carry bit from the accumulator (A) data 
 * using two's complement.
 * 
 * size:    2 bytes
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_SBI_D8                (unsigned char)0xde

/**
 * @def E8080_RST_3
 * @brief Intel 8080 RST opcode with special subroutine 3 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_3                 (unsigned char)0xdf

/**
 * @def E8080_RPO
 * @brief Intel 8080 RPO opcode
 * 
 * Performs a return operation if the Parity bit flag (P) is not set (odd parity).
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RPO                   (unsigned char)0xe0

/**
 * @def E8080_POP_H
 * @brief Intel 8080 POP opcode with HL registers pair as operand
 * 
 * Pop data off the stack in HL registers pair from the specified memory address by the stack pointer (SP).
 * The 8-bit data from SP is stored in the second register, the 8-bit data from SP+1 is stored in the
 * first register.
 * 
 * This operation will increment the stack pointer value by 2.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_POP_H                 (unsigned char)0xe1

/**
 * @def E8080_JPO_A16
 * @brief Intel 8080 JPO opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Parity bit flag (P) is not set (odd parity).
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JPO_A16               (unsigned char)0xe2

/**
 * @def E8080_XTHL
 * @brief Intel 8080 XTHL opcode
 * 
 * Exchange stack with HL registers pair content. The stack pointer (SP) will exchange data with
 * L register and SP+1 with H register.
 * 
 * size:    1 byte
 * time:    18 cycles
 * flags:   - - - - -
*/
#define E8080_XTHL                  (unsigned char)0xe3

/**
 * @def E8080_CPO_A16
 * @brief Intel 8080 CPO opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Parity bit flag (P) is not set (odd parity).
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CPO_A16               (unsigned char)0xe4

/**
 * @def E8080_PUSH_H
 * @brief Intel 8080 PUSH opcode with HL registers pair as operand
 * 
 * Push HL registers pair data on stack at memory address specified by stack pointer (SP).
 * The second register data will be pushed at SP-1 and the first register data pushed to SP-2.
 * 
 * This operation will decrement the stack pointer by 2.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - - 
*/
#define E8080_PUSH_H                (unsigned char)0xe5

/**
 * @def E8080_ANI_D8
 * @brief Intel 8080 ANI opcode with 8 bits immediate value as operand
 * 
 * AND accumulator (A) with 8 bit immediate value, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    2 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ANI_D8                 (unsigned char)0xe6

/**
 * @def E8080_RST_4
 * @brief Intel 8080 RST opcode with special subroutine 4 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_4                 (unsigned char)0xe7

/**
 * @def E8080_RPE
 * @brief Intel 8080 RPE opcode
 * 
 * Performs a return operation if the Parity bit flag (P) is set (even parity).
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RPE                   (unsigned char)0xe8

/**
 * @def E8080_PCHL
 * @brief Intel 8080 PCHL opcode
 * 
 * Loads HL registers pair value in program counter (PC) then continue execution to
 * the new address indicated by PC.
 * 
 * MSB will be loaded from H register, LSB will be loaded from L register.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_PCHL                  (unsigned char)0xe9

/**
 * @def E8080_JPE_A16
 * @brief Intel 8080 JPE opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Parity bit flag (P) is set (even parity).
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JPE_A16               (unsigned char)0xea

/**
 * @def E8080_XCHG
 * @brief Intel 8080 XCHG opcode
 * 
 * Exchanges the values of HL registers pair with DE registers pair one.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_XCHG                  (unsigned char)0xeb

/**
 * @def E8080_CPE_A16
 * @brief Intel 8080 CPE opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Parity bit flag (P) is set (even parity).
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CPE_A16               (unsigned char)0xec

/**
 * @def E8080_XRI_D8
 * @brief Intel 8080 XRI opcode with 8 bits immediate value as operand
 * 
 * XOR accumulator (A) with 8 bit immediate value, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    2 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_XRI_D8                 (unsigned char)0xee

/**
 * @def E8080_RST_5
 * @brief Intel 8080 RST opcode with special subroutine 5 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_5                 (unsigned char)0xef

/**
 * @def E8080_RP
 * @brief Intel 8080 RP opcode
 * 
 * Performs a return operation if the Sign bit flag (S) is not set (positive result).
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RP                   (unsigned char)0xf0

/**
 * @def E8080_POP_PSW
 * @brief Intel 8080 POP opcode with PSW (A+Status) registers pair as operand
 * 
 * Pop data off the stack in PSW registers pair from the specified memory address by the stack pointer (SP).
 * The 8-bit data from SP is stored in the second register, the 8-bit data from SP+1 is stored in the
 * first register.
 * 
 * In this case, SP+1 data will be used to restore the five condition bits.
 * 
 * This operation will increment the stack pointer value by 2.
 * 
 * size:    1 byte
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_POP_PSW                 (unsigned char)0xf1

/**
 * @def E8080_JP_A16
 * @brief Intel 8080 JP opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Sign bit flag (S) is not set (positive result).
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JP_A16               (unsigned char)0xf2

/**
 * @def E8080_DI
 * @brief Intel 8080 DI opcode
 * 
 * Disable interrupts.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - -
*/
#define E8080_DI                    (unsigned char)0xf3

/**
 * @def E8080_CP_A16
 * @brief Intel 8080 CP opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Sign bit flag (S) is not set (positive result).
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CP_A16               (unsigned char)0xf4

/**
 * @def E8080_PUSH_PSW
 * @brief Intel 8080 PUSH opcode with PSW (A+Status) registers pair as operand
 * 
 * Push PSW registers pair data on stack at memory address specified by stack pointer (SP).
 * The second register data will be pushed at SP-1 and the first register data pushed to SP-2.
 * 
 * This operation will decrement the stack pointer by 2.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - - 
*/
#define E8080_PUSH_PSW                (unsigned char)0xf5

/**
 * @def E8080_ORI_D8
 * @brief Intel 8080 ORI opcode with 8 bits immediate value as operand
 * 
 * OR accumulator (A) with 8 bit immediate value, result stored in accumulator (A).
 * This instruction reset the carry bit to zero.
 * 
 * size:    2 byte
 * time:    7 cycles
 * flags:   S Z A P C
*/
#define E8080_ORI_D8                 (unsigned char)0xf6

/**
 * @def E8080_RST_6
 * @brief Intel 8080 RST opcode with special subroutine 6 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_6                 (unsigned char)0xf7

/**
 * @def E8080_RM
 * @brief Intel 8080 RM opcode
 * 
 * Performs a return operation if the Sign bit flag (S) is set (negative result).
 * 
 * size:    1 byte
 * time:    11/5 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_RM                   (unsigned char)0xf8

/**
 * @def E8080_SPHL
 * @brief Intel 8080 SPHL opcode
 * 
 * Loads the stack pointer (SP) from HL registers pair.
 * 
 * size:    1 byte
 * time:    5 cycles
 * flags:   - - - - -
*/
#define E8080_SPHL                 (unsigned char)0xf9

/**
 * @def E8080_JM_A16
 * @brief Intel 8080 JM opcode with 16 bits memory address as operand
 * 
 * Program counter (PC) will points, for the next cycle, to the specified 16 bits memory address
 * if the Sign bit flag (S) is set (negative result).
 * 
 * size:    3 bytes
 * time:    10 cycles
 * flags:   - - - - -
*/
#define E8080_JM_A16               (unsigned char)0xfa

/**
 * @def E8080_EI
 * @brief Intel 8080 EI opcode
 * 
 * Enable interrupts.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   - - - - -
*/
#define E8080_EI                    (unsigned char)0xfb

/**
 * @def E8080_CM_A16
 * @brief Intel 8080 CM opcode with 16 bits memory address as operand
 * 
 * Performs a call operation to subroutine located at memory address specified by operand 
 * if the Sign bit flag (S) is set (negative result).
 * 
 * size:    3 bytes
 * time:    17/11 cycles [performed/not performed]
 * flags:   - - - - -
*/
#define E8080_CM_A16               (unsigned char)0xfc

/**
 * @def E8080_CPI_D8
 * @brief Intel 8080 CPI opcode with 8 bits immediate value as operand
 * 
 * Compares 8 bits immediate value with accumulator (A) one then set the Zero bit flag
 * if equality is true.
 * 
 * Accumulator (A) value remain unchanged.
 * 
 * This operation is performed by substracting the operand value with accumulator (A) one.
 * 
 * size:    1 byte
 * time:    4 cycles
 * flags:   S Z A P C
*/
#define E8080_CPI_D8                 (unsigned char)0xfe

/**
 * @def E8080_RST_7
 * @brief Intel 8080 RST opcode with special subroutine 7 operand
 * 
 * Push the program counter (PC) value to the stack to provides a return address after
 * special subroutine execution.
 * 
 * Theses are mostly called ISR (Interrupt Service Routines) and they are located in the first 64 words
 * of the memory.
 * 
 * This operation assumes that the called ISR will push then restore PSW registers pair (accumulator + flags)
 * before/after execution.
 * 
 * size:    1 byte
 * time:    11 cycles
 * flags:   - - - - -
*/
#define E8080_RST_7                 (unsigned char)0xff

/** @}*/

#endif