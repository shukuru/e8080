/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file errno.h
 * @brief Error definitions header file
*/

#ifndef E8080_ERROR_H
#define E8080_ERROR_H

#include <errno.h>
#include <string.h>

/**
 * @brief C standard errno declaration
*/
extern int errno;

/**
 * @brief e8080 emulator inner errno declaration
*/
extern int e8080_errno;

/**
 * @def E8080_SETERRNO
 * @brief Set emulator inner errno value
 * 
 * @param ec    Error code to set
*/
#define E8080_SETERRNO(ec) (e8080_errno = ec)

/**
 * @defgroup e8080ErrorsCodes e8080 emulator errors codes macro-definitions
 * 
 * Groups all emulator errors codes
 * 
 * @{
*/
/**
 * @def E8080_EADDRMAX
 * @brief Maximum addressable value reached
 * 
 * Defined by E8080_CPU_HIGH_ADDRESS macro-definition in cpu.h
*/
#define E8080_EADDRMAX      -1
/**
 * @}
*/

/**
 * @brief Returns e8080 emulator errno code associated string value
 * 
 * Computes e8080_strerrno absolute index from negative codes.
 * 
 * @todo More robust bitwise processing
 * 
 * @param code e8080 emulator errno value
 * 
 * @return e8080 errno associated string value
*/
const char* e8080_strerror(int code);

#endif