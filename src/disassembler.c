/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file disassembler.c
 * @brief Intel 8080 disassembler source file
*/

#include "disassembler.h"

unsigned char e8080_disassemble_opcode(unsigned char* opcode, unsigned short pc) {
    unsigned char* opcptr = opcode;
    unsigned char oplen = 0;

    _P_OPCODE"%04x\t\t", pc);

    switch(*opcptr) {
        case E8080_NOP:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_NOP); oplen = 1; break;
        case E8080_LXI_B_D16:   _P_OPCODE"%s B,%u\n", E8080_DISASSEMBLER_OPCODE_LXI, e8080_b8tb16(*(opcptr+1), *(opcptr+2))); oplen = 3; break;
        case E8080_STAX_B:      _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_STAX); oplen = 1; break;
        case E8080_INX_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_INX); oplen = 1; break;
        case E8080_INR_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_B_D8:    _P_OPCODE"%s B,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_RLC:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RLC); oplen = 1; break;
        case E8080_DAD_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_DAD); oplen = 1; break;
        case E8080_LDAX_B:      _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_LDAX); oplen = 1; break;
        case E8080_DCX_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_DCX); oplen = 1; break;
        case E8080_INR_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_C_D8:    _P_OPCODE"%s C,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_RRC:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RRC); oplen = 1; break;
        case E8080_LXI_D_D16:   _P_OPCODE"%s D,%u\n", E8080_DISASSEMBLER_OPCODE_LXI, e8080_b8tb16(*(opcptr+1), *(opcptr+2))); oplen = 3; break;
        case E8080_STAX_D:      _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_STAX); oplen = 1; break;
        case E8080_INX_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_INX); oplen = 1; break;
        case E8080_INR_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_D_D8:    _P_OPCODE"%s D,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_RAL:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RAL); oplen = 1; break;
        case E8080_DAD_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_DAD); oplen = 1; break;
        case E8080_LDAX_D:      _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_LDAX); oplen = 1; break;
        case E8080_DCX_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_DCX); oplen = 1; break;
        case E8080_INR_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_E_D8:    _P_OPCODE"%s E,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_RAR:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RAR); oplen = 1; break;
        case E8080_LXI_H_D16:   _P_OPCODE"%s H,%u\n", E8080_DISASSEMBLER_OPCODE_LXI, e8080_b8tb16(*(opcptr+1), *(opcptr+2))); oplen = 3; break;
        case E8080_SHLD_A16:    _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_SHLD, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_INX_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_INX); oplen = 1; break;
        case E8080_INR_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_H_D8:    _P_OPCODE"%s H,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_DAA:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_DAA); oplen = 1; break;
        case E8080_DAD_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_DAD); oplen = 1; break;
        case E8080_LHLD_A16:    _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_LHLD, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_DCX_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_DCX); oplen = 1; break;
        case E8080_INR_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_L_D8:    _P_OPCODE"%s L,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_CMA:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_CMA); oplen = 1; break;
        case E8080_LXI_SP_D16:  _P_OPCODE"%s SP,%u\n", E8080_DISASSEMBLER_OPCODE_LXI, e8080_b8tb16(*(opcptr+1), *(opcptr+2))); oplen = 3; break;
        case E8080_STA_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_STA, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_INX_SP:      _P_OPCODE"%s SP\n", E8080_DISASSEMBLER_OPCODE_INX); oplen = 1; break;
        case E8080_INR_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_M_D8:    _P_OPCODE"%s M,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_STC:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_STC); oplen = 1; break;
        case E8080_DAD_SP:      _P_OPCODE"%s SP\n", E8080_DISASSEMBLER_OPCODE_DAD); oplen = 1; break;
        case E8080_LDA_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_LDA, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_DCX_SP:      _P_OPCODE"%s SP\n", E8080_DISASSEMBLER_OPCODE_DCX); oplen = 1; break;
        case E8080_INR_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_INR); oplen = 1; break;
        case E8080_DCR_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_DCR); oplen = 1; break;
        case E8080_MVI_A_D8:    _P_OPCODE"%s A,%u\n", E8080_DISASSEMBLER_OPCODE_MVI, *(opcptr+1)); oplen = 2; break;
        case E8080_CMC:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_CMC); oplen = 1; break;
        case E8080_MOV_B_B:     _P_OPCODE"%s B,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_C:     _P_OPCODE"%s B,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_D:     _P_OPCODE"%s B,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_E:     _P_OPCODE"%s B,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_H:     _P_OPCODE"%s B,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_L:     _P_OPCODE"%s B,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_M:     _P_OPCODE"%s B,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_B_A:     _P_OPCODE"%s B,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_B:     _P_OPCODE"%s C,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_C:     _P_OPCODE"%s C,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_D:     _P_OPCODE"%s C,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_E:     _P_OPCODE"%s C,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_H:     _P_OPCODE"%s C,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_L:     _P_OPCODE"%s C,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_M:     _P_OPCODE"%s C,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_C_A:     _P_OPCODE"%s C,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_B:     _P_OPCODE"%s D,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_C:     _P_OPCODE"%s D,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_D:     _P_OPCODE"%s D,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_E:     _P_OPCODE"%s D,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_H:     _P_OPCODE"%s D,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_L:     _P_OPCODE"%s D,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_M:     _P_OPCODE"%s D,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_D_A:     _P_OPCODE"%s D,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_B:     _P_OPCODE"%s E,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_C:     _P_OPCODE"%s E,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_D:     _P_OPCODE"%s E,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_E:     _P_OPCODE"%s E,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_H:     _P_OPCODE"%s E,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_L:     _P_OPCODE"%s E,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_M:     _P_OPCODE"%s E,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_E_A:     _P_OPCODE"%s E,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_B:     _P_OPCODE"%s H,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_C:     _P_OPCODE"%s H,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_D:     _P_OPCODE"%s H,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_E:     _P_OPCODE"%s H,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_H:     _P_OPCODE"%s H,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_L:     _P_OPCODE"%s H,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_M:     _P_OPCODE"%s H,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_H_A:     _P_OPCODE"%s H,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_B:     _P_OPCODE"%s L,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_C:     _P_OPCODE"%s L,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_D:     _P_OPCODE"%s L,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_E:     _P_OPCODE"%s L,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_H:     _P_OPCODE"%s L,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_L:     _P_OPCODE"%s L,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_M:     _P_OPCODE"%s L,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_L_A:     _P_OPCODE"%s L,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_M_B:     _P_OPCODE"%s M,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_M_C:     _P_OPCODE"%s M,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_M_D:     _P_OPCODE"%s M,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_M_E:     _P_OPCODE"%s M,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_M_H:     _P_OPCODE"%s M,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_M_L:     _P_OPCODE"%s M,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_HLT:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_HLT); oplen = 1; break;
        case E8080_MOV_M_A:     _P_OPCODE"%s M,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_B:     _P_OPCODE"%s A,B\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_C:     _P_OPCODE"%s A,C\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_D:     _P_OPCODE"%s A,D\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_E:     _P_OPCODE"%s A,E\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_H:     _P_OPCODE"%s A,H\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_L:     _P_OPCODE"%s A,L\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_M:     _P_OPCODE"%s A,M\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_MOV_A_A:     _P_OPCODE"%s A,A\n", E8080_DISASSEMBLER_OPCODE_MOV); oplen = 1; break;
        case E8080_ADD_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADD_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_ADD); oplen = 1; break;
        case E8080_ADC_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_ADC_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_ADC); oplen = 1; break;
        case E8080_SUB_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SUB_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_SUB); oplen = 1; break;
        case E8080_SSB_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_SSB_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_SSB); oplen = 1; break;
        case E8080_ANA_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_ANA_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_ANA); oplen = 1; break;
        case E8080_XRA_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_XRA_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_XRA); oplen = 1; break;
        case E8080_ORA_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_ORA_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_ORA); oplen = 1; break;
        case E8080_CMP_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_C:       _P_OPCODE"%s C\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_E:       _P_OPCODE"%s E\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_L:       _P_OPCODE"%s L\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_M:       _P_OPCODE"%s M\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_CMP_A:       _P_OPCODE"%s A\n", E8080_DISASSEMBLER_OPCODE_CMP); oplen = 1; break;
        case E8080_RNZ:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RNZ); oplen = 1; break;
        case E8080_POP_B:       _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_POP); oplen = 1; break;
        case E8080_JNZ_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JNZ, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_JMP_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JMP, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_CNZ_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CNZ, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_PUSH_B:      _P_OPCODE"%s B\n", E8080_DISASSEMBLER_OPCODE_PUSH); oplen = 1; break;
        case E8080_ADI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_ADI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_0:       _P_OPCODE"%s 0\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RZ:          _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RZ); oplen = 1; break;
        case E8080_RET:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RET); oplen = 1; break;
        case E8080_JZ_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JZ, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_CZ_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CZ, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_CALL_A16:    _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CALL, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_ACI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_ACI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_1:       _P_OPCODE"%s 1\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RNC:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RNC); oplen = 1; break;
        case E8080_POP_D:       _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_POP); oplen = 1; break;
        case E8080_JNC_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JNC, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_OUT_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_OUT, *(opcptr+1)); oplen = 2; break;
        case E8080_CNC_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CNC, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_PUSH_D:      _P_OPCODE"%s D\n", E8080_DISASSEMBLER_OPCODE_PUSH); oplen = 1; break;
        case E8080_SUI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_SUI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_2:       _P_OPCODE"%s 2\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RC:          _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RC); oplen = 1; break;
        case E8080_JC_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JC, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_IN_D8:       _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_IN, *(opcptr+1)); oplen = 2; break;
        case E8080_CC_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CC, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_SBI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_SBI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_3:       _P_OPCODE"%s 3\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RPO:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RPO); oplen = 1; break;
        case E8080_POP_H:       _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_POP); oplen = 1; break;
        case E8080_JPO_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JPO, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_XTHL:        _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_XTHL); oplen = 1; break;
        case E8080_CPO_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CPO, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_PUSH_H:      _P_OPCODE"%s H\n", E8080_DISASSEMBLER_OPCODE_PUSH); oplen = 1; break;
        case E8080_ANI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_ANI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_4:       _P_OPCODE"%s 4\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RPE:         _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RPE); oplen = 1; break;
        case E8080_PCHL:        _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_PCHL); oplen = 1; break;
        case E8080_JPE_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JPE, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_XCHG:        _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_XCHG); oplen = 1; break;
        case E8080_CPE_A16:     _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CPE, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_XRI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_XRI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_5:       _P_OPCODE"%s 5\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RP:          _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RP); oplen = 1; break;
        case E8080_POP_PSW:     _P_OPCODE"%s PSW\n", E8080_DISASSEMBLER_OPCODE_POP); oplen = 1; break;
        case E8080_JP_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JP, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_DI:          _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_DI); oplen = 1; break;
        case E8080_CP_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CP, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_PUSH_PSW:    _P_OPCODE"%s PSW\n", E8080_DISASSEMBLER_OPCODE_PUSH); oplen = 1; break;
        case E8080_ORI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_ORI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_6:       _P_OPCODE"%s 6\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;
        case E8080_RM:          _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_RM); oplen = 1; break;
        case E8080_SPHL:        _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_SPHL); oplen = 1; break;
        case E8080_JM_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_JM, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_EI:          _P_OPCODE"%s\n", E8080_DISASSEMBLER_OPCODE_EI); oplen = 1; break;
        case E8080_CM_A16:      _P_OPCODE"%s %02x%02xh\n", E8080_DISASSEMBLER_OPCODE_CM, *(opcptr+1), *(opcptr+2)); oplen = 3; break;
        case E8080_CPI_D8:      _P_OPCODE"%s %u\n", E8080_DISASSEMBLER_OPCODE_CPI, *(opcptr+1)); oplen = 2; break;
        case E8080_RST_7:       _P_OPCODE"%s 7\n", E8080_DISASSEMBLER_OPCODE_RST); oplen = 1; break;

        default:                _P_OPCODE"???\n"); oplen = 1; break;
    }

    return oplen;
}

unsigned short e8080_disassemble(unsigned char* buf, unsigned short buflen) {
    unsigned short pc;

    while(pc < buflen)
        pc += e8080_disassemble_opcode((buf+pc), pc);

    return pc;
}