/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file io.c
 * @brief Emulator I/O processing source file
*/

#include "io.h"

/**
 * @brief Computes a file stream size
 * 
 * @param fp    File stream
 * 
 * @return File stream size
*/
static size_t e8080_fssz(FILE* fp) {
    assert(fp);

    size_t p_len = 0;

    fseek(fp, 0L, SEEK_END);
    p_len = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    return p_len;
}

size_t e8080_pread(unsigned char* buf, FILE* fp) {
    assert(buf); assert(fp);
    
    size_t p_len = e8080_fssz(fp);

    if(p_len > E8080_CPU_HIGH_ADDRESS) {
        E8080_SETERRNO(E8080_EADDRMAX);
        return 0;
    }

    return fread(buf, sizeof(unsigned char), p_len, fp);
}