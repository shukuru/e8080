/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file e8080.c
 * @brief Intel 8080 entrypoint source file
*/

#include "disassembler.h"
#include "io.h"

#ifndef E8080_SEMVER
#define E8080_MAJOR     0
#define E8080_MINOR     0
#define E8080_PATCH     0
#endif

/**
 * @brief Holds program short arguments
*/
static char* e8080_program_short_args[] = {
    "-d",
};

/**
 * @brief Holds program long arguments
*/
static char* e8080_program_long_args[] = {
    "--disassemble"
};

static const char* _program_usage = "usage: e8080 [ARG] [ARGS] [ARG=value]\n\n\
\t-d [FILE],--disassemble [FILE]: Disassemble e8080 executable program\n\
";

/**
 * @def _P_USAGE
 * @brief Prints program usage message
*/
#define _P_USAGE    fprintf(stderr, "e8080 %d.%d.%d\n%s\n", E8080_MAJOR, E8080_MINOR, E8080_PATCH, _program_usage)

int main(int argc, char** argv) {
    if(3 > argc) {
        _P_USAGE;
        return 2;
    }

    if(!strcmp(*e8080_program_short_args, *(argv+1)) || !strcmp(*e8080_program_long_args, *(argv+1))) {
        FILE* fp = fopen(*(argv+2), "rb");

        if(!fp) {
            fprintf(stderr, "error: %s\n", strerror(errno));
            return errno;
        }

        unsigned char program[0xffff] = {0x00};

        fprintf(stdout, "Reading %s Intel 8080 program...", *(argv+2));
        size_t program_sz = e8080_pread(program, fp);
        
        if(!program_sz) {
            fprintf(stdout, "\n\033[31mERROR\033[0m: %s\n", e8080_strerror(e8080_errno));
            fclose(fp);
            return e8080_errno;
        }

        fprintf(stdout, "\t\033[32mOK\033[0m (Read %zu bytes)\n", program_sz);

        fclose(fp);

        fprintf(stdout, "Disassembling program...\n\n");
        e8080_disassemble(program, program_sz);

        fprintf(stdout, "Done.\n");
        return 0;
    }

    _P_USAGE;
    return 2;
}