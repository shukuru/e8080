/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file disassembler_test.c
 * @brief Intel 8080 disassembler test unit source file
*/

#include "testing/testing.h"

#include "opcodes.h"
#include "disassembler.h"

int e8080_disassemble_opcode_test(void* arg) {
    if(arg)
        e8080_tprintf("this test unit doesn't need any arguments\n");

    printf("[?] e8080_disassemble_opcode_test\n\n");

    unsigned int pc = 0x00;

    unsigned char opcode_1b = 0x02;
    unsigned char opcode_2b[2] = {0x06, 0xa0};
    unsigned char opcode_3b_d16[3] = {0x01, 0xff, 0x0a};
    unsigned char opcode_3b_a16[3] = {0x22, 0xff, 0x0a};

    #define _EXPECTED_PC (sizeof opcode_1b + sizeof opcode_2b + sizeof opcode_3b_d16 + sizeof opcode_3b_a16) 

    e8080_tprintf("disassembler pc value: 0x%04x\n", (pc += e8080_disassemble_opcode(&opcode_1b, pc)));
    e8080_tprintf("disassembler pc value: 0x%04x\n", (pc += e8080_disassemble_opcode(opcode_2b, pc)));
    e8080_tprintf("disassembler pc value: 0x%04x\n", (pc += e8080_disassemble_opcode(opcode_3b_d16, pc)));
    e8080_tprintf("disassembler pc value: 0x%04x\n", (pc += e8080_disassemble_opcode(opcode_3b_a16, pc)));

    e8080_tprintf("Expected result: 0x%04x - Got: 0x%04x\n", _EXPECTED_PC, pc);

    return !(_EXPECTED_PC - pc);
}

int e8080_disassemble_test(void* arg) {
    if(arg)
        e8080_tprintf("this test unit doesn't need any arguments\n");

    printf("[?] e8080_disassemble_test\n\n");

    unsigned int pc = 0x00;
    unsigned char program[16] = {
        0x00, 0x00, 0x00, 0xc3, 0xd4, 0x18, 0x00, 0x00,
        0xf5, 0xc5, 0xd5, 0xe5, 0xc3, 0x8c, 0x00, 0x00
    };

    pc += e8080_disassemble(program, sizeof program);

    e8080_tprintf("Expected result: 0x%04x - Got: 0x%04x\n", sizeof program, pc);

    return!(sizeof program - pc);
}
