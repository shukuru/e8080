/*
    Copyright (c) 2020-2022 Florent <IOFrog> DUPONT

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file testing.c
 * @brief Intel 8080 testing source file
*/

#include "testing/testing.h"

/* Test units must be declared extern right below this comment */
extern int e8080_disassemble_opcode_test(void*);
extern int e8080_disassemble_test(void*);
extern int e8080_b8tb16_test(void*);

/**
 * @brief Passed test units counter
*/
static int _test_units_passed = 0;

/**
 * @brief Failed test units counter
*/
static int _test_units_failed = 0;

/**
 * @brief Test units array
 * 
 * Test units callbacks array.
 * Each callback must be declared extern before registering it in this array
 * Array must be terminated with NULL value because gcc always put the _IO_2_1_stdout_ function
 * address right after the array boundaries and cause segfault.
 * 
 * @todo Find why gcc does that and a more elegant way to avoid this
*/
static int (*_tests_list[])(void*) = {
    e8080_disassemble_opcode_test,
    e8080_disassemble_test,
    e8080_b8tb16_test,
    NULL
};

void e8080_tprintf(const char* fmt, ...) {
    va_list v_args;

    printf("@test unit@\t");

    va_start(v_args, fmt);
    vprintf(fmt, v_args);
    va_end(v_args);

    return;
}

void e8080_trunall() {
    size_t tcount = 0; for(; *(_tests_list+tcount); tcount++);

    printf("\033[34m== e8080 emulator testing units run ==\033[0m\n\n");
    printf("\033[33m[Tests info]\033[0m\n");
    printf("+ Start time: %lu\n+ To run: %zu %s\n\n---\n\n", time(NULL), tcount, !(1 - tcount) ? "tests" : "test");

    for(size_t i = 0; i < tcount; i++) {
        printf("\033[33m[Running test unit \033[0m#%zu\033[33m]\033[0m\n", i+1);
        int (*test)(void*) = *(_tests_list+i);
        test(NULL) ? (printf("\n\033[32mPASS\033[0m\n\n"), ++_test_units_passed) : (printf("\n\033[31mFAIL\033[0m\n\n"), ++_test_units_failed);
    }

    printf("---\n\n\033[33m[Final result]\033[0m\n+ End time: %lu\n+ Total pass: %d\n+ Total fail: %d\n", time(NULL), _test_units_passed, _test_units_failed);

    return;
}

int main(void) {
    e8080_trunall();
    return 0;
}